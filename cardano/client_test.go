package cardano_test

import (
	"testing"

	"gitlab.com/nunet/tokenomics-api/nunet-cardano-access/cardano"
	"gitlab.com/nunet/tokenomics-api/nunet-cardano-access/models"
)

var queryUtxoOutput = `{
    "00879a13024a238bde88032b2204ed9318826dd3fbee795f12b9e3368fcb1000#0": {
        "address": "addr_test1wp2nthmgs7n6cwfs4srjs8vtsayhss08he0vwyl2e0v836s5n6jgy",
        "datum": null,
        "inlineDatum": {
            "constructor": 0,
            "fields": [
                {
                    "bytes": "2a8768a44dfaba7919235f7bb8ac263a5a13f0aae441dceaa2e15326"
                },
                {
                    "bytes": "e6e3d34872336fe10ed47ee9a4e1f7399f0eb8aad32d2e80a1ac1def"
                },
                {
                    "int": 4
                },
                {
                    "bytes": "d6aa0d489c13c54f70ed080d1d10bb75bce975c93b812eb12adc69b9b6aeb190e146f309906611ad1746264de9d60ee1524c7725fde20bd01e12ebf51c75f109"
                },
                {
                    "bytes": "66756e64696e672d62275820765c7839315c7862365c7866335923424a3f4b5c786131466d5c783934395c7866355c7864305c7839324d4d735c7863665c783965285c7831395c7861635c7862316e41345c7865625c78663527"
                },
                {
                    "bytes": ""
                },
                {
                    "bytes": ""
                },
                {
                    "bytes": ""
                }
            ]
        },
        "inlineDatumhash": "eaf8644822280c51545b6e4dc359ff05f2645f14820e9278bea20bb6325a7fc7",
        "referenceScript": null,
        "value": {
            "8cafc9b387c9f6519cacdce48a8448c062670c810d8da4b232e56313": {
                "6d4e5458": 4
            },
            "lovelace": 2167930
        }
    },
    "015aae6088c9799fbbbb1e4470bb6b7a175ecbec7db384336e635d2a28859de9#0": {
        "address": "addr_test1wp2nthmgs7n6cwfs4srjs8vtsayhss08he0vwyl2e0v836s5n6jgy",
        "datum": null,
        "inlineDatum": {
            "constructor": 0,
            "fields": [
                {
                    "bytes": "9e6bb3e091e12fc131131eb3100b39a51f1a550da04ffbfd5ead4000"
                },
                {
                    "bytes": "d412f0ccf74bb61d8b25662f9639fe70174b1cf98668f8576ef7e460"
                },
                {
                    "int": 2
                },
                {
                    "bytes": "92a34f93773e2345a0b71be129ea53ffcb99c3e16c711f6965c8b4935f12af949a6526e23ce4daf10d4f29f913c09326d745036bba9747bea20cefd4b7efe90c"
                },
                {
                    "bytes": "66756e64696e672d622758205c783963745c7863342f365c786534245c7863657c5c783132315c7866365c7864305c7863335c786432515c7864337b5b5c7862355c7862615c783962435c7863345c7838656e5c7864365c7838612f5c7865315c7863387227"
                },
                {
                    "bytes": ""
                },
                {
                    "bytes": ""
                },
                {
                    "bytes": ""
                }
            ]
        },
        "inlineDatumhash": "5cd08ad9e0f5382a99df460ed0a8c1723aa9ff7671f082aa0d7b8a7b5504809a",
        "referenceScript": null,
        "value": {
            "8cafc9b387c9f6519cacdce48a8448c062670c810d8da4b232e56313": {
                "6d4e5458": 2
            },
            "lovelace": 2219650
        }
    }
}`

func TestFetchUtxoAndDatumFromOutput(t *testing.T) {
	utxos, datum, err := cardano.FetchUtxoAndDatumFromOutput([]byte(queryUtxoOutput))
	if err != nil {
		t.Error(err)
	}

	assertUtxosCount(t, utxos, 2)
	assertDatumCount(t, datum, 2)

	assertUtxoQuantity(t, utxos[0], "2167930", "4")
	assertUtxoQuantity(t, utxos[1], "2219650", "2")

	assertDatumField(t, datum[0], 0, "2a8768a44dfaba7919235f7bb8ac263a5a13f0aae441dceaa2e15326")
	assertDatumField(t, datum[0], 1, "e6e3d34872336fe10ed47ee9a4e1f7399f0eb8aad32d2e80a1ac1def")
	assertDatumFieldInt(t, datum[0], 2, 4)
}

func assertUtxosCount(t *testing.T, utxos []models.UTXOOut, expectedCount int) {
	if len(utxos) != expectedCount {
		t.Errorf("Expected %d utxos, got %d", expectedCount, len(utxos))
	}
}

func assertDatumCount(t *testing.T, datum []models.InlineDatum, expectedCount int) {
	if len(datum) != expectedCount {
		t.Errorf("Expected %d datum, got %d", expectedCount, len(datum))
	}
}

func assertUtxoQuantity(t *testing.T, utxo models.UTXOOut, lovelace, quantity string) {
	if utxo.Output.Amount[0].Quantity != lovelace {
		t.Errorf("Expected %s, got %s", lovelace, utxo.Output.Amount[0].Quantity)
	}

	if utxo.Output.Amount[1].Quantity != quantity {
		t.Errorf("Expected %s, got %s", quantity, utxo.Output.Amount[1].Quantity)
	}
}

func assertDatumField(t *testing.T, data models.InlineDatum, index int, expected string) {
	if data.Fields[index].Bytes != expected {
		t.Errorf("Expected %s, got %s", expected, data.Fields[index].Bytes)
	}
}

func assertDatumFieldInt(t *testing.T, data models.InlineDatum, index, expected int) {
	if data.Fields[index].Int != expected {
		t.Errorf("Expected %d, got %d", expected, data.Fields[index].Int)
	}
}
