package cardano

import (
	"encoding/json"
	"errors"
	"fmt"
	"os/exec"
	"strconv"
	"strings"

	"github.com/spf13/viper"
	"gitlab.com/nunet/tokenomics-api/nunet-cardano-access/models"
)

var (
	assetIdentifier = "8cafc9b387c9f6519cacdce48a8448c062670c810d8da4b232e56313"
	mntxHash        = "6d4e5458"
)

type CardanoInterface interface {
	QueryUTXOs(scriptAddress, env string) ([]models.UTXOOut, []models.InlineDatum, error)
}

var RealCardano CardanoInterface // Real implementation

func QueryUTXOs(address string, env string) (utxos []models.UTXOOut, datum []models.InlineDatum, err error) {
	cardanoCliAbsPath := viper.GetString("CARDANO_CLI_PATH")

	// Create the command to run
	cmdArgs, err := createQueryUtxoCommand(address, env)
	if err != nil {
		return nil, nil, err
	}

	cmd := exec.Command(cardanoCliAbsPath, cmdArgs...)
	output, err := cmd.CombinedOutput()
	if err != nil {
		return nil, nil, err
	}

	utxos, datum, err = FetchUtxoAndDatumFromOutput(output)
	if err != nil {
		return nil, nil, err
	}

	return utxos, datum, nil
}

func createQueryUtxoCommand(address string, env string) (cmdArgs []string, err error) {
	var envFlag string
	var socketPath string
	var magicValue string

	// set the appropriate env flag based on the environment
	switch env {
	case "mainnet":
		envFlag = "--mainnet"
		socketPath = viper.GetString("MAINNET_SOCKET_PATH")
	case "testnet":
		envFlag = "--testnet-magic"
		socketPath = viper.GetString("PREPROD_SOCKET_PATH")
		magicValue = "1"
	default:
		return cmdArgs, errors.New("unsupported environment")
	}

	cmdArgs = []string{"query", "utxo", "--address", address, envFlag}
	if env == "testnet" {
		cmdArgs = append(cmdArgs, magicValue) // add the magic value to the args
	}
	cmdArgs = append(cmdArgs, "--socket-path", socketPath)
	cmdArgs = append(cmdArgs, "--out-file", "/dev/stdout")

	return cmdArgs, nil
}

// FetchUtxoAndDatumFromOutput parses the output from the `cardano-cli query utxo` command and returns a slice of UTXOOut and InlineDatum
func FetchUtxoAndDatumFromOutput(output []byte) (utxo []models.UTXOOut, datum []models.InlineDatum, err error) {
	var data map[string]models.UTXOIn
	var outUtxo []models.UTXOOut
	var outDatum []models.InlineDatum

	err = json.Unmarshal([]byte(output), &data)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}

	for k, v := range data {
		keyParts := strings.Split(k, "#")
		txHash := keyParts[0]
		outputIndex := keyParts[1]
		outputIndexInt, err := strconv.Atoi(outputIndex)
		if err != nil {
			fmt.Println("Error converting outputIndex to int:", err)
			continue
		}

		var finalUTXO models.UTXOOut
		finalUTXO.Input.TxHash = txHash
		finalUTXO.Input.OutputIndex = outputIndexInt

		finalUTXO.Output.Address = v.Address
		finalUTXO.Output.Amount = []models.Amount{
			{
				Unit:     "lovelace",
				Quantity: fmt.Sprintf("%d", v.Value.Lovelace),
			},
			{
				Unit:     fmt.Sprintf("%s%s", assetIdentifier, mntxHash),
				Quantity: fmt.Sprintf("%d", v.Value.AssetIdentifier[mntxHash]),
			},
		}

		outUtxo = append(outUtxo, finalUTXO)
		outDatum = append(outDatum, v.InlineDatum)
	}
	return outUtxo, outDatum, nil
}
