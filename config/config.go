package config

import (
	"github.com/spf13/viper"
)

func InitConfig() error {
	// Set the path and name of the .env file
	viper.SetConfigFile(".env")

	// Read in the environment variables
	if err := viper.ReadInConfig(); err != nil {
		return err
	}

	// Optionally, set defaults or do additional configurations here

	return nil
}
