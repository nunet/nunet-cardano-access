package models

// Bytes struct
type Bytes struct {
	Bytes string `json:"bytes"`
}

// Int struct
type Int struct {
	Int int `json:"int"`
}

// Fields struct
type Fields struct {
	Bytes string `json:"bytes,omitempty"`
	Int   int    `json:"int,omitempty"`
}

// Value struct
type Value struct {
	AssetIdentifier map[string]int `json:"8cafc9b387c9f6519cacdce48a8448c062670c810d8da4b232e56313"`
	Lovelace        int            `json:"lovelace"`
}

// InlineDatum struct
type InlineDatum struct {
	Constructor int      `json:"constructor"`
	Fields      []Fields `json:"fields"`
}

// UTXOIn struct
type UTXOIn struct {
	Address         string      `json:"address"`
	Datum           interface{} `json:"datum"`
	InlineDatum     InlineDatum `json:"inlineDatum"`
	InlineDatumHash string      `json:"inlineDatumhash"`
	ReferenceScript interface{} `json:"referenceScript"`
	Value           Value       `json:"value"`
}

// out struct definitions

type Amount struct {
	Unit     string `json:"unit"`
	Quantity string `json:"quantity"`
}

type Output struct {
	Address string   `json:"address"`
	Amount  []Amount `json:"amount"`
}

type Input struct {
	OutputIndex int    `json:"outputIndex"`
	TxHash      string `json:"txHash"`
}

type UTXOOut struct {
	Input  Input  `json:"input"`
	Output Output `json:"output"`
}

type UTXORequest struct {
	ScriptAddress string `json:"scriptAddress"`
	Env           string `json:"env"`
}

type UTXOResponse struct {
	UTXOs  []UTXOOut     `json:"utxo"`
	Datums []InlineDatum `json:"datum"`
}
