package main

import (
	"fmt"
	"log"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"gitlab.com/nunet/tokenomics-api/nunet-cardano-access/config"
	_ "gitlab.com/nunet/tokenomics-api/nunet-cardano-access/docs"
	"gitlab.com/nunet/tokenomics-api/nunet-cardano-access/handlers"
)

func init() {
	// Initialize Viper to read from the .env file
	if err := config.InitConfig(); err != nil {
		log.Fatalf("Error initializing config: %s", err)
	}
}

//	@title			Cardano Access API
//	@version		1.0.0
//	@description	Cardano Access is a wrapper around cardano-cli.

//	@contact.name	NuNet Support
//	@contact.url	https://discord.gg/pg5BnFM89n
//	@contact.email	support@nunet.io

//	@license.name	GPL 3.0
//	@license.url	https://www.gnu.org/licenses/agpl-3.0.en.html

//	@host		localhost:8080
//	@BasePath	/api/v1

// @externalDocs.description	Technical Documentation
// @externalDocs.url			https://docs.nunet.io/nunet-public-alpha-testnet/
func main() {
	r := gin.Default()
	r.Use(cors.New(getCustomCorsConfig()))

	// Create a group with the prefix "/api/v1"
	v1 := r.Group("/api/v1")

	// Define routes within the "/api/v1" group
	v1.POST("/utxo", handlers.CheckUTXO)

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	// Run the server
	port := viper.GetString("PORT")
	r.Run(fmt.Sprintf(":%s", port))
}

func getCustomCorsConfig() cors.Config {
	config := DefaultConfig()
	// FIXME: This is a security concern.
	config.AllowOrigins = []string{"*"}
	return config
}

// DefaultConfig returns a generic default configuration mapped to localhost.
func DefaultConfig() cors.Config {
	return cors.Config{
		AllowMethods:     []string{"GET", "POST", "PUT", "PATCH", "DELETE", "HEAD", "OPTIONS"},
		AllowHeaders:     []string{"Access-Control-Allow-Origin", "Origin", "Content-Length", "Content-Type"},
		AllowCredentials: false,
		MaxAge:           12 * time.Hour,
	}
}
