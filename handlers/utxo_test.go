package handlers_test

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"

	"gitlab.com/nunet/tokenomics-api/nunet-cardano-access/cardano"
	"gitlab.com/nunet/tokenomics-api/nunet-cardano-access/handlers"
	"gitlab.com/nunet/tokenomics-api/nunet-cardano-access/models"
)

type MockCardano struct{}

func (m *MockCardano) QueryUTXOs(scriptAddress, env string) ([]models.UTXOOut, []models.InlineDatum, error) {
	return []models.UTXOOut{}, []models.InlineDatum{}, nil
}

func TestCheckUTXO_Success(t *testing.T) {
	gin.SetMode(gin.TestMode)

	// Create a mock gin context with a request
	reqBody := `{"scriptAddress": "testScriptAddress", "env": "testnet"}`
	req, err := http.NewRequest("POST", "/api/v1/utxo", strings.NewReader(reqBody))
	if err != nil {
		t.Fatal(err)
	}
	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request = req

	// Inject the mock implementation into the handler
	cardano.RealCardano = &MockCardano{}
	defer func() {
		cardano.RealCardano = nil // Clean up mock after test
	}()

	// Call the handler function
	handlers.CheckUTXO(c)

	var response models.UTXOResponse
	err = json.Unmarshal(w.Body.Bytes(), &response)
	assert.NoError(t, err)
	// Add assertions for the response content as needed
}
func TestCheckUTXO_BadRequest(t *testing.T) {
	gin.SetMode(gin.TestMode)

	// Create a mock gin context with an invalid request
	reqBody := `{}` // Invalid request body without required fields
	req, err := http.NewRequest("POST", "/api/v1/utxo", strings.NewReader(reqBody))
	if err != nil {
		t.Fatal(err)
	}
	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request = req

	// Inject the mock implementation into the handler
	cardano.RealCardano = &MockCardano{}
	defer func() {
		cardano.RealCardano = nil // Clean up mock after test
	}()

	// Call the handler function
	handlers.CheckUTXO(c)

	// Verify the response
	assert.Equal(t, http.StatusBadRequest, w.Code)

	var response map[string]string
	err = json.Unmarshal(w.Body.Bytes(), &response)
	assert.NoError(t, err)
	assert.Contains(t, response["error"], "missing required fields")
}

func TestCheckUTXO_UnsupportedEnvironment(t *testing.T) {
	gin.SetMode(gin.TestMode)

	// Create a mock gin context with a valid request
	reqBody := `{"scriptAddress": "validAddress", "env": "invalidEnv"}`
	req, err := http.NewRequest("POST", "/api/v1/utxo", strings.NewReader(reqBody))
	if err != nil {
		t.Fatal(err)
	}
	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request = req

	// Mock the cardano.QueryUTXOs function to return an error
	cardano.RealCardano = &MockCardano{} // Using the mock for this test case
	defer func() {
		cardano.RealCardano = nil // Clean up mock after test
	}()

	// Call the handler function
	handlers.CheckUTXO(c)

	// Verify the response
	assert.Equal(t, http.StatusInternalServerError, w.Code)

	var response map[string]string
	err = json.Unmarshal(w.Body.Bytes(), &response)
	assert.NoError(t, err)
	assert.Contains(t, response["error"], "unsupported environment")
}
