package handlers

import (
	"net/http"

	"gitlab.com/nunet/tokenomics-api/nunet-cardano-access/cardano"
	"gitlab.com/nunet/tokenomics-api/nunet-cardano-access/models"

	"github.com/gin-gonic/gin"
)

// CheckUTXO godoc
//
//	@Summary		Query UTXOs
//	@Description	Query UTXOs for a given address
//	@Tags			UTXO
//	@Accept			json
//	@Produce		json
//	@Param			body	body		models.UTXORequest	true	"Transaction Request"
//	@Response		200		{object}	models.UTXOResponse	"UTXO"
//	@Response		400		{string}	string				"Bad Request"
//	@Response		500		{string}	string				"Internal Server Error"
//	@Router			/api/v1/utxo [post]
func CheckUTXO(c *gin.Context) {
	var req models.UTXORequest
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if req.Env == "" || req.ScriptAddress == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "missing required fields"})
		return
	}

	utxos, datum, err := cardano.QueryUTXOs(req.ScriptAddress, req.Env)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	// put outUtxo and outDatum in a container and print it as json
	response := models.UTXOResponse{
		UTXOs:  utxos,
		Datums: datum,
	}

	c.JSON(http.StatusOK, response)
}
