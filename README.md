# nunet-cardano-access

Nunet Cardano Access is a NuNet wrapper around cardano-node and cardano-cli. It provides a REST API to interact with the Cardano blockchain.

It implements functionalities related to Cardano blockchain that will be used on NuNet to compensate the device providers.


## Setup

1. Make sure you have go, cardano-node and cardano-cli installed.
2. Clone the repo.
3. Copy `.env.example` to `.env` and fill in the values.

Set the values appropriately for your environment. Default one is just for demonstration.

4. Run `go run main.go` to start the HTTP server on port.


## API

### POST /api/v1/utxo

#### Request

```json
{
    "scriptAddress": "addr_test1qrfsxut2r7jvf7de599xecntwrqhnp5la6fksdmvkanzzh36wk0gkawrszrskwhy2yr5ylerugsghvtxkczhm2gww6xs6zvulx",
    "env": "testnet"
}
```

* `scriptAddress`: The wallet/contract address
* `env`: testnet or mainnet

#### Response

Output is an object which has `utxo` and `datum` fields.

Both fields are a list of objects. First object in utxo list corresponds to the first object in datum list and so on.

Here is a sample response. Please note that output is limited to 1 utxo and 1 datum object:

```json
{
  "utxo": [
      {
          "input": {
              "outputIndex": 0,
              "txHash": "766384d50b309270a59795d6140e39f84e9af25ab2a2a365ba40ce135209ffff"
          },
          "output": {
              "address": "addr_test1wp2nthmgs7n6cwfs4srjs8vtsayhss08he0vwyl2e0v836s5n6jgy",
              "amount": [
                  {
                      "unit": "lovelace",
                      "quantity": "2629100"
                  },
                  {
                      "unit": "8cafc9b387c9f6519cacdce48a8448c062670c810d8da4b232e563136d4e5458",
                      "quantity": "1"
                  }
              ]
          }
      }
  ],
  "datum": [
      {
          "constructor": 0,
          "fields": [
              {
                  "bytes": "07286573e75596d0dd60f85b30a61143bead14edc74e31331077ef7e"
              },
              {
                  "bytes": "e6e3d34872336fe10ed47ee9a4e1f7399f0eb8aad32d2e80a1ac1def"
              },
              {
                  "int": 1
              },
              {
                  "bytes": "616464725f74657374317171726a7365746e7561326564357861767275396b7639787a39706d61746735616872357576666e7a706d37376c3530613263677172307130643930736e7038703470616c6e376e33763538713871733936383777686139646b7a717473327a7a67616464725f746573743171726e77383536677767656b6c63677736336c776e66387037757565377234633474666a3674357135786b706d6d366174647077706532643468746735687733716d347476376b336a396364756c737a3468746471687a36753371736b6b3536647030"
              },
              {
                  "bytes": "7573737175627177666e"
              },
              {
                  "bytes": "7866636a6e79747a7776"
              },
              {
                  "bytes": "667a646f74716e726d76"
              },
              {
                  "bytes": "666168696a716a616e70"
              }
          ]
      }
  ]
}
```
